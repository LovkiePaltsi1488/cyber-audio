import { createMuiTheme } from "@material-ui/core/styles";

// Create a theme instance.
const theme = createMuiTheme({
	palette: {
		background: {
			default: "#fff",
		},
	},
	typography: {
		fontFamily: "'Permanent Marker', cursive",
		fontWeightRegular: 400,
		fontWeightBold: 600,
	},
	overrides: {
		MuiCssBaseline: {
			"@global": {
				body: {
					display: "flex",
					margin: "0",
					padding: "0",
					boxSizing: "border-box",
					minHeight: "100vh",
				},
				"#__next": {
					width: "100%",
					display: "flex",
					alignItems: "center",
					flex: 1,
					backgroundColor: "#212121",
					padding: "20px 0",
				},
				"#ripple": {
					position: "absolute",
					borderRadius: "50%",
					transform: "scale(0)",
					animation: "$ripple 800ms linear",
					backgroundColor: "rgba(255, 255, 255, 0.7)",
				},
				"@keyframes ripple": {
					to: {
						transform: "scale(4)",
						opacity: 0,
					},
				},
				"@media (max-width: 1000px)": {
					"#__next": {
						padding: "5% 0",
					},
				},
				"@media (max-width: 500px)": {
					"#__next": {
						padding: "0",
					},
				},
			},
		},
	},
});

export default theme;
