import React, { ReactNode, useState } from "react";
import Head from "next/head";
import { useStyles } from "./styles";
import { NavigationDrawer } from "./Navigation/Navigation";
import { Button, IconButton } from "@material-ui/core";
import { Menu, ExitToApp } from "@material-ui/icons";
import { setLogout } from "../../middleware/utils";
export interface AppLayoutProps {
	title: string;
	children?: ReactNode;
}
export const AppLayout: React.FC<AppLayoutProps> = ({ title, children }) => {
	const classes = useStyles();
	const [opened, setOpened] = useState(false);
	function openHandler() {
		setOpened(!opened);
	}
	return (
		<div className={classes.app}>
			<Head>
				<title>{title}</title>
			</Head>
			<NavigationDrawer opened={opened} />
			<div
				onClick={openHandler}
				className={
					classes.drawer__backdrop +
					` ${opened ? classes.drawer__backdrop_visible : ""}`
				}
			></div>
			<div
				className={classes.app__container}
				style={{ flexDirection: "column" }}
			>
				<menu className={classes["app-menu"]}>
					<IconButton
						aria-label="menu"
						classes={{ root: classes.buttonRoot }}
						onClick={() => openHandler()}
					>
						<Menu className={classes["app-menu__icon"]} />
					</IconButton>
					<Button
						variant="contained"
						classes={{
							root: classes.exitButtonRoot,
						}}
						startIcon={<ExitToApp className={classes["app-menu__icon"]} />}
						onClick={setLogout}
					>
						Log out
					</Button>
				</menu>
				<div
					className={
						classes.app__container + " " + classes.app__container_scrollable
					}
				>
					{children}
				</div>
				<div className={classes["app-hole"]}></div>
			</div>
		</div>
	);
};
