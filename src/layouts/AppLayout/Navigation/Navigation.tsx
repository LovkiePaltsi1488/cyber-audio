import React from "react";
import {
	Home,
	LibraryMusic,
	QueueMusic,
	Search,
	Star,
	Album,
	Publish,
	Favorite,
} from "@material-ui/icons";
import { SmartLink } from "./Link/Link";
import { useStyles } from "./styles";
export interface NavigationDrawerProps {
	opened: boolean;
}
export const NavigationDrawer: React.FC<NavigationDrawerProps> = ({
	opened,
}) => {
	const classes = useStyles();
	return (
		<div className={classes.drawer + ` ${opened ? classes.drawer_opened : ""}`}>
			<h1 className={classes.logo}>Cyber-Audio</h1>
			<div className={classes.drawer__container}>
				<nav className={classes["drawer-nav"]}>
					<SmartLink
						href={"/"}
						className={classes["drawer-nav__link"]}
						active={classes["drawer-nav__link_active"]}
					>
						<Home className={classes["drawer-nav__icon"]} />
						Home
					</SmartLink>
					<SmartLink
						href={"/newstuff"}
						className={classes["drawer-nav__link"]}
						active={classes["drawer-nav__link_active"]}
					>
						<LibraryMusic className={classes["drawer-nav__icon"]} />
						New stuff
					</SmartLink>
					<SmartLink
						href={"/popular"}
						className={classes["drawer-nav__link"]}
						active={classes["drawer-nav__link_active"]}
					>
						<Star className={classes["drawer-nav__icon"]} />
						Popular
					</SmartLink>
					<SmartLink
						href={"/search"}
						className={classes["drawer-nav__link"]}
						active={classes["drawer-nav__link_active"]}
					>
						<Search className={classes["drawer-nav__icon"]} />
						Search
					</SmartLink>
				</nav>
				<nav className={classes["drawer-nav"]}>
					<SmartLink
						href={"/library"}
						className={
							classes["drawer-nav__link"] +
							" " +
							classes["drawer-nav__link_green"]
						}
						active={
							classes["drawer-nav__link_active"] +
							" " +
							classes["drawer-nav__link_green_active"]
						}
					>
						<QueueMusic className={classes["drawer-nav__icon"]} />
						Library
					</SmartLink>
					<SmartLink
						href={"/playlists"}
						className={
							classes["drawer-nav__link"] +
							" " +
							classes["drawer-nav__link_green"]
						}
						active={
							classes["drawer-nav__link_active"] +
							" " +
							classes["drawer-nav__link_green_active"]
						}
					>
						<Album className={classes["drawer-nav__icon"]} />
						Playlists
					</SmartLink>
					<SmartLink
						href={"/favorite"}
						className={
							classes["drawer-nav__link"] +
							" " +
							classes["drawer-nav__link_green"]
						}
						active={
							classes["drawer-nav__link_active"] +
							" " +
							classes["drawer-nav__link_green_active"]
						}
					>
						<Favorite className={classes["drawer-nav__icon"]} />
						Favorite
					</SmartLink>
					<SmartLink
						href={"/loadmusic"}
						className={
							classes["drawer-nav__link"] +
							" " +
							classes["drawer-nav__link_green"]
						}
						active={
							classes["drawer-nav__link_active"] +
							" " +
							classes["drawer-nav__link_green_active"]
						}
					>
						<Publish className={classes["drawer-nav__icon"]} />
						Load Music
					</SmartLink>
				</nav>
			</div>
		</div>
	);
};
