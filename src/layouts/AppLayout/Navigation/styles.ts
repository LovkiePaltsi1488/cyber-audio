import { makeStyles } from "@material-ui/core";

export const useStyles = makeStyles((theme) => ({
	drawer: {
		flex: 1,
		maxWidth: "25%",
		height: "100%",
		backgroundColor: "rgba(0, 0, 0, 0.9)",
		display: "flex",
		flexDirection: "column",
		boxSizing: "border-box",
		borderRight: "1px solid #b8b8b8",
		paddingBottom: "60px",
		position: "relative",
		transition: "0.5s",
	},
	drawer_opened: {
		transform: "translateX(0px)",
	},
	drawer__container: {
		display: "flex",
		flexDirection: "column",
		flex: 1,
		marginRight: "5px",
		overflowY: "auto",
		"&::-webkit-scrollbar": {
			background: "transparent",
			borderRadius: "10px",
			width: "2px",
		},
		"&::-webkit-scrollbar-thumb": {
			background:
				"linear-gradient(135deg, rgba(230,81,0,1) 0%, rgba(255,183,77,1) 100%)",
			borderRadius: "2px",
			width: "2px",
		},
	},
	logo: {
		color: "#fff3e0",
		boxSizing: "border-box",
		textAlign: "center",
		fontSize: "30px",
		fontWeight: 600,
		marginTop: "20px",
		marginBottom: "20px",
		WebkitUserSelect: "none",
		letterSpacing: 2,
		textShadow:
			"0 0 0.6rem #fff3e0, 0 0 1.5rem #ffab40, -0.2rem 0.1rem 1rem #ffab40, 0.2rem 0.1rem 1rem #ffab40, 0 -0.5rem 2rem #ff6d00, 0 0.5rem 3rem #ff6d00",
	},
	["drawer-nav"]: {
		display: "flex",
		flexDirection: "column",
		flex: 1,
		alignItems: "center",
	},
	["drawer-nav__icon"]: {
		marginLeft: "15px",
		marginRight: "15px",
		width: "30px",
		height: "30px",
	},
	["drawer-nav__link"]: {
		color: "rgba(255, 255, 255, 0.5)",
		width: "80%",
		padding: "5px 0px",
		textAlign: "center",
		fontSize: "20px",
		display: "flex",
		alignItems: "center",
		borderRadius: "10px",
		boxSizing: "border-box",
		margin: "10px 0",
		transition: "0.5s",
		textDecoration: "none",
		"&:hover": {
			boxShadow: "0 0 10px #ffb74d",
			color: "#ffb74d",
		},
	},
	["drawer-nav__link_active"]: {
		background:
			"linear-gradient(135deg, rgba(230,81,0,1) 0%, rgba(255,183,77,1) 100%)",
		boxShadow: "0 0 10px #ffb74d",
		cursor: "default",
		color: "#fff",
		"&:hover": {
			color: "#fff",
		},
	},
	["drawer-nav__link_green"]: {
		"&:hover": {
			boxShadow: "0 0 10px #DCE775",
			color: "#DCE775",
		},
	},
	["drawer-nav__link_green_active"]: {
		background:
			"linear-gradient(135deg, rgba(130,119,23,1) 0%, rgba(220,231,117,1) 100%)",
		boxShadow: "0 0 10px #DCE775",
		cursor: "default",
		color: "#fff",
		"&:hover": {
			color: "#fff",
		},
	},
	["@media (max-width: 1000px)"]: {
		logo: {
			fontSize: "20px",
		},
		["drawer-nav__icon"]: {
			margin: "0px 8px",
			width: "20px",
			height: "20px",
		},
		["drawer-nav__link"]: {
			fontSize: "16px",
			margin: "5px 0px",
		},
	},
	["@media (max-width: 700px)"]: {
		drawer: {
			maxWidth: "250px",
			width: "100%",
			position: "absolute",
			left: "-250px",
			top: "0",
			zIndex: "5",
			backgroundColor: "#000000",
		},
		drawer_opened: {
			transform: "translateX(250px)",
		},
		logo: {
			fontSize: "30px",
		},
		["drawer-nav__icon"]: {
			margin: "0px 15px",
			width: "30px",
			height: "30px",
		},
		["drawer-nav__link"]: {
			fontSize: "20px",
			margin: "10px 0px",
		},
	},
}));

/* ,
	["drawer-nav__link_music"]: {
		"&:hover": {
			boxShadow: "0 0 10px #DCE775",
			color: "#DCE775",
		},
	},
	["drawer-nav__link_music_active"]: {
		background:
			"linear-gradient(135deg, rgba(130,119,23,1) 0%, rgba(220,231,117,1) 100%)",
		boxShadow: "0 0 10px #DCE775",
		cursor: "default",
		color: "#fff",
		"&:hover": {
			color: "#fff",
		},
	}, */

/* ["drawer-nav__link_search"]: {
  "&:hover": {
    boxShadow: "0 0 10px #4DB6AC",
    color: "#4DB6AC",
  },
},
["drawer-nav__link_search_active"]: {
  background:
    "linear-gradient(135deg, rgba(0,77,64,1) 0%, rgba(77,182,172,1) 100%)",
  boxShadow: "0 0 10px #4DB6AC",
  cursor: "default",
  color: "#fff",
  "&:hover": {
    color: "#fff",
  },
}, */
