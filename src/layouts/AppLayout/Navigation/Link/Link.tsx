import React, { ReactNode } from "react";
import Link from "next/link";
import { useRouter } from "next/router";

export interface SmartLinkProps {
	href: string;
	className: string;
	children: ReactNode;
	active: string;
}
export const SmartLink: React.FC<SmartLinkProps> = ({
	href,
	children,
	className,
	active,
}) => {
	const router = useRouter();

	if (router.pathname === href) {
		className = `${className} ${active}`;
	}
	// {React.cloneElement(children, { className })}
	return (
		<Link href={href}>
			<a className={className}>{children}</a>
		</Link>
	);
};
