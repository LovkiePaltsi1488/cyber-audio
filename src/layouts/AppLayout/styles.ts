import { makeStyles } from "@material-ui/core";

export const useStyles = makeStyles((theme) => ({
	app: {
		position: "relative",
		backgroundColor: "#000000",
		margin: "0 auto",
		maxWidth: "90%",
		maxHeight: "90vh",
		minHeight: "600px",
		flex: 1,
		boxSizing: "border-box",
		borderRadius: "10px",
		boxShadow: "0px 0px 8px rgba(0, 0, 0, 0.5)",
		height: "100%",
		display: "flex",
		overflow: "hidden",
	},
	app__container: {
		display: "flex",
		flex: 1,
		alignItems: "center",
	},
	app__container_scrollable: {
		marginRight: "5px",
		width: "100%",
		overflowY: "auto",
		flexDirection: "column",
		padding: "20px 0",
		"&::-webkit-scrollbar": {
			background: "transparent",
			borderRadius: "10px",
			width: "2px",
		},
		"&::-webkit-scrollbar-thumb": {
			background:
				"linear-gradient(135deg, rgba(230,81,0,1) 0%, rgba(255,183,77,1) 100%)",
			borderRadius: "2px",
			width: "2px",
		},
	},
	["app-menu"]: {
		display: "flex",
		flex: 1,
		maxHeight: "60px",
		boxSizing: "border-box",
		/* borderBottom: "1px solid #b8b8b8", */
		padding: "5px",
		paddingRight: "9px",
		margin: "0",
		width: "100%",
		justifyContent: "flex-end",
		alignItems: "center",
	},
	drawer__backdrop: {
		backgroundColor: "rgba(255, 255, 255, 0.3)",
		position: "absolute",
		zIndex: 2,
		top: 0,
		left: "0",
		right: "0",
		bottom: "0",
		display: "none",
		transition: "1s",
	},
	drawer__backdrop_visible: {
		display: "none",
	},
	buttonRoot: {
		color: "white",
		"& .MuiTouchRipple-root": {
			color: "white",
		},
		display: "none",
	},
	exitButtonRoot: {
		background:
			"linear-gradient(135deg, rgba(230,81,0,1) 0%, rgba(255,183,77,1) 100%)",
		color: "white",
		"&:hover": {
			boxShadow: "0 0 10px #ffb74d",
		},
		"& .MuiButton-label": {
			color: "#fff",
		},
	},
	["app-menu__icon"]: {
		color: "#fff",
		width: "30px",
		height: "30px",
	},
	["app-hole"]: {
		display: "flex",
		flex: 1,
		maxHeight: "60px",
		boxSizing: "border-box",
		/* borderTop: "1px solid #b8b8b8", */
		padding: "0",
		width: "100%",
	},
	"@media (max-width: 1000px)": {
		app: {
			maxHeight: "88vh",
			minHeight: "400px",
		},
		["app-menu"]: {
			maxHeight: "68px",
		},
		["app-hole"]: {
			maxHeight: "68px",
		},
	},
	"@media (max-width: 700px)": {
		buttonRoot: {
			display: "flex",
		},
		["app-menu"]: {
			justifyContent: "space-between",
		},
		drawer__backdrop_visible: {
			display: "block",
		},
	},
	"@media (max-width: 500px)": {
		app: {
			maxWidth: "100%",
			maxHeight: "100%",
			boxShadow: "none",
			border: "none",
			borderRadius: "0",
		},
	},
}));
