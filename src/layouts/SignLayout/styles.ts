import { makeStyles } from "@material-ui/core";

export const useStyles = makeStyles((theme) => ({
	app: {
		position: "relative",
		backgroundColor: "#000000",
		margin: "0 auto",
		maxWidth: "90%",
		maxHeight: "90%",
		minHeight: "600px",
		flex: 1,
		boxSizing: "border-box",
		borderRadius: "10px",
		boxShadow: "0px 0px 8px rgba(0, 0, 0, 0.5)",
		height: "100%",
		paddingBottom: "50px",
	},
	"@media (max-width: 1000px)": {
		app: {
			maxHeight: "100%",
			minHeight: "400px",
		},
	},
	"@media (max-width: 500px)": {
		app: {
			maxWidth: "100%",
			boxShadow: "none",
			border: "none",
			borderRadius: "0",
		},
	},
}));
