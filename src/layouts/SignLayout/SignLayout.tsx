import React, { ReactNode } from "react";
import Head from "next/head";
import { useStyles } from "./styles";
export interface AppLayoutProps {
	title: string;
	children?: ReactNode;
}
export const SignLayout: React.FC<AppLayoutProps> = ({ title, children }) => {
	const classes = useStyles();
	return (
		<div className={classes.app}>
			<Head>
				<title>{title}</title>
			</Head>
			{children}
		</div>
	);
};
