import React, { useState } from "react";
import { AppLayout } from "../layouts/AppLayout/AppLayout";
import { Loader } from "./Loader/Loader";
import { useStyles } from "./styles";
export const App: React.FC<{}> = () => {
	const [loading, setLoading] = useState(true);
	const classes = useStyles();
	return (
		<AppLayout title="Cyber-Audio | Home">{loading && <Loader />}</AppLayout>
	);
};
