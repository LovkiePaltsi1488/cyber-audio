import { TextField } from "@material-ui/core";
import Router from "next/router";
import React, { ChangeEvent, useState, MouseEvent } from "react";
import { createRipple } from "../../utils/appearence";
import { Error, musicValidator } from "../../utils/validator";
import { Loader } from "../Loader/Loader";
import { FilePicker } from "./FilePicker/FilePicker";
import { useStyles } from "./styles";

export const SongForm: React.FC<{}> = () => {
	const classes = useStyles();
	const [loading, setLoading] = useState(false);
	const [title, setTitle] = useState("");
	const [author, setAuthor] = useState("");
	const [error, setError] = useState(false);
	const [titleError, setTitleError] = useState<Error>({
		error: false,
		text: "",
	});
	const [authorError, setAuthorError] = useState<Error>({
		error: false,
		text: "",
	});
	const [image, setImage] = useState<File>();
	const [track, setTrack] = useState<File>();
	const [trackError, setTrackError] = useState<Error>({
		error: false,
		text: "",
	});
	async function sendTrack(e: MouseEvent<HTMLButtonElement>) {
		checkForm();
		setErrors();
		if (musicValidator.valid) {
			const formData = new FormData();
			formData.append("title", title.trim());
			formData.append("author", author.trim());
			image && formData.append("image", image);
			formData.append("audio", track);
			setLoading(true);
			await fetch("/api/loadmusic", {
				method: "POST",
				body: formData,
			}).then((response) => checkResponse(response));
		}
	}
	function checkForm() {
		musicValidator.clear();
		musicValidator.checkAuthor(author.trim());
		musicValidator.checkTitle(title.trim());
		musicValidator.checkSong(track);
	}
	function setErrors() {
		setAuthorError({
			error: !!musicValidator.authorError,
			text: musicValidator.authorError,
		});
		setTitleError({
			error: !!musicValidator.titleError,
			text: musicValidator.titleError,
		});
		setTrackError({
			error: !!musicValidator.songError,
			text: musicValidator.songError,
		});
		setError(!musicValidator.valid);
	}
	function clearForm() {
		setError(false);
		setAuthor("");
		setTitle("");
		setTrack(undefined);
		setImage(undefined);
		setTitleError({
			error: false,
			text: "",
		});
		setAuthorError({
			error: false,
			text: "",
		});
		setTrackError({
			error: false,
			text: "",
		});
	}
	function checkResponse(response: Response) {
		clearForm();
		setLoading(false);
		if (response.status === 401) {
			Router.push("/signin");
		}
	}
	return (
		<>
			{loading && (
				<>
					<Loader />
				</>
			)}
			{!loading && (
				<>
					<div
						className={classes.songForm__inner}
						style={{ marginBottom: "20px" }}
					>
						<FilePicker
							file={image}
							setFile={setImage}
							type={{ name: "image", accept: "image/*" }}
						/>
					</div>
					<div className={classes.songForm__inner}>
						<TextField
							placeholder="Type song title"
							onChange={(e: ChangeEvent<HTMLInputElement>) =>
								setTitle(e.target.value)
							}
							value={title}
							label="Title"
							variant="outlined"
							error={titleError.error}
							helperText={titleError.text}
							classes={{
								root: classes.textField,
							}}
							InputLabelProps={{
								shrink: true,
							}}
						></TextField>
					</div>
					<div className={classes.songForm__inner}>
						<TextField
							placeholder="Type song author"
							onChange={(e: ChangeEvent<HTMLInputElement>) =>
								setAuthor(e.target.value)
							}
							value={author}
							label="Author"
							variant="outlined"
							error={authorError.error}
							helperText={authorError.text}
							classes={{
								root: classes.textField,
							}}
							InputLabelProps={{
								shrink: true,
							}}
						></TextField>
					</div>
					<div
						className={
							classes.songForm__inner + " " + classes.songForm__inner_extended
						}
					>
						<FilePicker
							file={track}
							setFile={setTrack}
							error={trackError.error}
							helperText={trackError.text}
							type={{ name: "audio", accept: "audio/*" }}
						/>
					</div>
					<div
						className={
							classes.songForm__inner + " " + classes.songForm__inner_extended
						}
					>
						<button
							onClick={(e: MouseEvent<HTMLButtonElement>) => {
								createRipple(e);
								sendTrack(e);
							}}
							className={
								classes.songForm__button +
								` ${error ? classes.songForm__button_error : ""}`
							}
						>
							Load audiofile
						</button>
					</div>
				</>
			)}
		</>
	);
};
