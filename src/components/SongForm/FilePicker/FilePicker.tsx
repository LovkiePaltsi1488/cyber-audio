import React, { ChangeEvent, SyntheticEvent, useRef } from "react";
import { createRipple } from "../../../utils/appearence";
import { useStyles } from "./styles";

export interface FilePickerProps {
	file: File;
	setFile: (file: File) => void;
	type: {
		name: string;
		accept: string;
	};
	error?: boolean;
	helperText?: string;
}
export const FilePicker: React.FC<FilePickerProps> = ({
	file,
	setFile,
	type,
	error,
	helperText,
}) => {
	const classes = useStyles();
	const ref = useRef<HTMLImageElement>(null);
	function changeHandler(e: ChangeEvent<HTMLInputElement>) {
		if (type.name === "image") {
			if (!!e.target.files[0]) {
				ref.current.src = URL.createObjectURL(e.target.files[0]);
			} else {
				ref.current.src = "./images/add_photo.svg";
			}
		}
		setFile(e.target.files[0]);
	}
	function onLoadImage(e: SyntheticEvent<HTMLImageElement>) {
		URL.revokeObjectURL(ref.current.src);
	}
	return (
		<>
			<div
				className={
					classes.filePicker +
					` ${type.name === "audio" ? classes.filePicker_audio : ""}` +
					` ${
						type.name === "audio" && !!file
							? classes.filePicker_audio_loaded
							: ""
					}` +
					` ${
						type.name === "image" && !!file
							? classes.filePicker_image_loaded
							: ""
					}` +
					` ${error ? classes.filePicker_error : ""}`
				}
				onClick={createRipple}
			>
				{type.name === "audio" && (
					<div className={classes.filePicker__audioName}>
						{file
							? file.name.length > 26
								? file.name.slice(0, 26)
								: file.name
							: "Load audio"}
					</div>
				)}
				<input
					type="file"
					onChange={changeHandler}
					accept={type.accept}
					multiple={false}
					className={
						classes.filePicker__input +
						` ${type.name === "audio" ? classes.filePicker__input_audio : ""}`
					}
				/>
				{type.name === "image" && (
					<img
						ref={ref}
						src={"./images/add_photo.svg"}
						onLoad={onLoadImage}
						className={
							classes.filePicker__image +
							` ${!!file ? "" : classes.filePicker__image_shrinked}`
						}
					/>
				)}
			</div>
			{helperText && (
				<p
					className={
						classes.filePicker__helperText +
						` ${error ? classes.filePicker__helperText_error : ""}`
					}
				>
					{helperText}
				</p>
			)}
		</>
	);
};
