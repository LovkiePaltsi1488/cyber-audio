import { makeStyles } from "@material-ui/core";

export const useStyles = makeStyles(() => ({
	filePicker: {
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
		width: "200px",
		height: "200px",
		position: "relative",
		justifyContent: "center",
		borderRadius: "10px",
		transition: "0.5s",
		"&:hover": {
			boxShadow: "0 0 20px #ffb74d",
		},
		overflow: "hidden",
		border: "1px solid rgba(255, 255, 255, 0.5)",
	},
	filePicker_error: {
		border: "1px solid #f44336",
	},
	filePicker_image_loaded: {
		border: "none",
	},
	filePicker_audio: {
		height: "60px",
		width: "300px",
		color: "rgba(255, 255, 255, 0.5)",
		"&:hover": {
			color: "rgba(255,183,77,1)",
		},
		background: "black",
		boxSizing: "border-box",
	},
	filePicker_audio_loaded: {
		background:
			"linear-gradient(135deg, rgba(230,81,0,1) 0%, rgba(255,183,77,1) 100%)",
		color: "#fff",
		"&:hover": {
			color: "#fff",
		},
		border: "none",
	},
	filePicker__image: {
		height: "200px",
		width: "200px",
		borderRadius: "10px",
		objectFit: "cover",
		border: "none",
		pointerEvents: "none",
		userSelect: "none",
		transition: "0.5s",
	},
	filePicker__image_shrinked: {
		width: "100px",
		height: "100px",
		opacity: "0.5",
	},
	filePicker__input: {
		opacity: 0,
		outline: "none",
		position: "absolute",
		width: "200px",
		height: "200px",
		borderRadius: "10px",
		cursor: "pointer",
		"&::-webkit-file-upload-button": {
			display: "none",
		},
		WebkitTapHighlightColor: "rgba(255, 255, 255, 0)",
	},
	filePicker__input_audio: {
		height: "100%",
		width: "100%",
	},
	filePicker__audioName: {
		pointerEvents: "none",
		userSelect: "none",
		width: "300px",
		height: "60px",
		justifyContent: "center",
		fontSize: "18px",
		borderRadius: "10px",
		color: "inherit",
		display: "flex",
		alignItems: "center",
		/* padding: "10px 40px", */
	},
	filePicker__helperText: {
		margin: "3px 14px 0",
		fontSize: "12px",
		color: "#fff",
	},
	filePicker__helperText_error: {
		color: "#d50000",
	},
	"@media (hover: none) and (pointer: coarse)": {
		filePicker: {
			color: "rgba(255, 255, 255, 0.5)",
			"&:hover": {
				boxShadow: "none",
				color: "rgba(255, 255, 255, 0.5)",
			},
		},
		filePicker_audio_loaded: {
			color: "rgba(255, 255, 255, 1) !important",
		},
	},
}));
