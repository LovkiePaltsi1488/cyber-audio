import { makeStyles } from "@material-ui/core";

export const useStyles = makeStyles(() => ({
	songForm: {
		display: "flex",
		flexDirection: "column",
		boxSizing: "border-box",
		width: "100%",
		height: "100%",
		padding: "20px 0",
	},
	songForm__inner: {
		display: "flex",
		flex: 1,
		boxSizing: "border-box",
		justifyContent: "center",
		marginTop: "5px",
		flexDirection: "column",
	},
	songForm__inner_extended: {
		minHeight: "85px",
		justifyContent: "flex-start",
	},
	textField: {
		"& label": {
			color: "rgba(255, 255, 255, 0.5)",
			transition: "0.5s",
			"&.Mui-focused": {
				color: "#ffb74d",
			},
		},
		"&:hover label": {
			color: "white",
		},
		"& .MuiOutlinedInput-root": {
			transition: "0.5s",
			borderRadius: "10px",
			"&:hover fieldset": {
				borderColor: "white",
			},
			"&.Mui-focused fieldset": {
				borderWidth: "1px",
				borderColor: "#ffb74d",
			},
			"& input": {
				fontSize: "18px",
				lineHeight: "19px",
				color: "rgba(255, 255, 255)",
				letterSpacing: 1.3,
			},
		},
		"& .MuiOutlinedInput-notchedOutline": {
			borderColor: "rgba(255, 255, 255, 0.5)",
			transition: "0.5s",
		},
		"& .MuiFormHelperText-root": {
			color: "#d50000",
		},
		height: "80px",
		width: "300px",
		marginTop: "10px",
	},
	songForm__button: {
		width: "300px",
		height: "60px",
		/* background: "linear-gradient(135deg, #004D40 0%, #4DB6AC 100%)", */
		background: "linear-gradient(135deg, #1B5E20 0%, #81C784 100%)",
		/* background: "linear-gradient(135deg, #D50000 0%, #E57373 100%)", */
		border: "none",
		borderRadius: "10px",
		color: "#fff",
		fontSize: "18px",
		transition: "0.5s",
		cursor: "pointer",
		fontFamily: "inherit",
		"&:hover": {
			boxShadow: "0 0 20px #81C784",
		},
		margin: "0",
		padding: "0",
		outline: "none",
		position: "relative",
		overflow: "hidden",
	},
	songForm__button_error: {
		background: "linear-gradient(135deg, #D50000 0%, #E57373 100%)",
		"&:hover": {
			boxShadow: "0 0 20px #E57373",
		},
	},
	"@media (hover: none) and (pointer: coarse)": {
		songForm__button: {
			"&:hover": {
				boxShadow: "none",
			},
		},
	},
}));
