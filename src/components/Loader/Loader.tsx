import React, { CSSProperties } from "react";
import classes from "./styles.module.scss";

export interface LoaderProps {
	style?: CSSProperties;
}
export const Loader: React.FC<LoaderProps> = ({ style }) => {
	return (
		<div className={classes["dank-ass-loader"]} style={style}>
			<div className={classes.row}>
				<div
					className={
						classes.arrow +
						" " +
						classes.up +
						" " +
						classes.outer +
						" " +
						classes["outer-18"]
					}
				></div>
				<div
					className={
						classes.arrow +
						" " +
						classes.down +
						" " +
						classes.outer +
						" " +
						classes["outer-17"]
					}
				></div>
				<div
					className={
						classes.arrow +
						" " +
						classes.up +
						" " +
						classes.outer +
						" " +
						classes["outer-16"]
					}
				></div>
				<div
					className={
						classes.arrow +
						" " +
						classes.down +
						" " +
						classes.outer +
						" " +
						classes["outer-15"]
					}
				></div>
				<div
					className={
						classes.arrow +
						" " +
						classes.up +
						" " +
						classes.outer +
						" " +
						classes["outer-14"]
					}
				></div>
			</div>
			<div className={classes.row}>
				<div
					className={
						classes.arrow +
						" " +
						classes.up +
						" " +
						classes.outer +
						" " +
						classes["outer-1"]
					}
				></div>
				<div
					className={
						classes.arrow +
						" " +
						classes.down +
						" " +
						classes.outer +
						" " +
						classes["outer-2"]
					}
				></div>
				<div
					className={
						classes.arrow +
						" " +
						classes.up +
						" " +
						classes.inner +
						" " +
						classes["inner-6"]
					}
				></div>
				<div
					className={
						classes.arrow +
						" " +
						classes.down +
						" " +
						classes.inner +
						" " +
						classes["inner-5"]
					}
				></div>
				<div
					className={
						classes.arrow +
						" " +
						classes.up +
						" " +
						classes.inner +
						" " +
						classes["inner-4"]
					}
				></div>
				<div
					className={
						classes.arrow +
						" " +
						classes.down +
						" " +
						classes.outer +
						" " +
						classes["outer-13"]
					}
				></div>
				<div
					className={
						classes.arrow +
						" " +
						classes.up +
						" " +
						classes.outer +
						" " +
						classes["outer-12"]
					}
				></div>
			</div>
			<div className={classes.row}>
				<div
					className={
						classes.arrow +
						" " +
						classes.down +
						" " +
						classes.outer +
						" " +
						classes["outer-3"]
					}
				></div>
				<div
					className={
						classes.arrow +
						" " +
						classes.up +
						" " +
						classes.outer +
						" " +
						classes["outer-4"]
					}
				></div>
				<div
					className={
						classes.arrow +
						" " +
						classes.down +
						" " +
						classes.inner +
						" " +
						classes["inner-1"]
					}
				></div>
				<div
					className={
						classes.arrow +
						" " +
						classes.up +
						" " +
						classes.inner +
						" " +
						classes["inner-2"]
					}
				></div>
				<div
					className={
						classes.arrow +
						" " +
						classes.down +
						" " +
						classes.inner +
						" " +
						classes["inner-3"]
					}
				></div>
				<div
					className={
						classes.arrow +
						" " +
						classes.up +
						" " +
						classes.outer +
						" " +
						classes["outer-11"]
					}
				></div>
				<div
					className={
						classes.arrow +
						" " +
						classes.down +
						" " +
						classes.outer +
						" " +
						classes["outer-10"]
					}
				></div>
			</div>
			<div className={classes.row}>
				<div
					className={
						classes.arrow +
						" " +
						classes.down +
						" " +
						classes.outer +
						" " +
						classes["outer-5"]
					}
				></div>
				<div
					className={
						classes.arrow +
						" " +
						classes.up +
						" " +
						classes.outer +
						" " +
						classes["outer-6"]
					}
				></div>
				<div
					className={
						classes.arrow +
						" " +
						classes.down +
						" " +
						classes.outer +
						" " +
						classes["outer-7"]
					}
				></div>
				<div
					className={
						classes.arrow +
						" " +
						classes.up +
						" " +
						classes.outer +
						" " +
						classes["outer-8"]
					}
				></div>
				<div
					className={
						classes.arrow +
						" " +
						classes.down +
						" " +
						classes.outer +
						" " +
						classes["outer-9"]
					}
				></div>
			</div>
		</div>
	);
};
