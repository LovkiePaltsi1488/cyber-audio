import React, { ChangeEvent, FormEvent, useState } from "react";
import { useStyles } from "./styles";
import { Button, TextField } from "@material-ui/core";
import { validator, Error } from "../../../utils/validator";
import Cookies from "js-cookie";
import Router from "next/router";
import { Loader } from "../../Loader/Loader";
import Link from "next/link";
export const SignUp = () => {
	const classes = useStyles();
	const [loading, setLoading] = useState(false);
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [nickname, setNickname] = useState("");
	const [emailError, setEmailError] = useState<Error>({
		error: false,
		text: "",
	});
	const [passwordError, setPasswordError] = useState<Error>({
		error: false,
		text: "",
	});
	const [nicknameError, setNicknameError] = useState<Error>({
		error: false,
		text: "",
	});
	function checkForm() {
		validator.clear();
		validator.checkEmail(email);
		validator.checkPassword(password);
		validator.checkNickname(nickname);
		setEmailError({
			error: !!validator.emailError,
			text: validator.emailError,
		});
		setPasswordError({
			error: !!validator.passwordError,
			text: validator.passwordError,
		});
		setNicknameError({
			error: !!validator.nicknameError,
			text: validator.nicknameError,
		});
	}
	async function submitHandler(e: FormEvent<HTMLFormElement>) {
		e.preventDefault();
		checkForm();
		console.log(validator);
		if (validator.valid) {
			setLoading(true);
			const loginApi = await fetch(`/api/auth`, {
				method: "POST",
				headers: {
					Accept: "application/json",
					"Content-Type": "application/json",
				},
				body: JSON.stringify({
					email,
					password,
					nickname,
				}),
			}).catch((error) => {
				console.error("Error:", error);
			});
			const result = await (loginApi as Response).json();
			if (result.success && result.token) {
				Cookies.set("token", result.token, {
					expires: 30,
				});
				// window.location.href = referer ? referer : "/";
				// const pathUrl = referer ? referer.lastIndexOf("/") : "/";
				Router.push("/");
			} else {
				setEmailError(result.email);
				setPasswordError(result.password);
				setLoading(false);
			}
		}
	}
	return (
		<form className={classes.signIn} onSubmit={submitHandler}>
			<h1 className={classes.signIn__header}>Cyber-Audio</h1>
			{loading && <Loader style={{ marginBottom: "100px" }} />}
			{!loading && (
				<div className={classes.SignIn__inner}>
					<TextField
						placeholder="Type your email"
						label="Email"
						error={emailError.error}
						helperText={emailError.text}
						onChange={(e: ChangeEvent<HTMLInputElement>) =>
							setEmail(e.target.value.trim())
						}
						value={email}
						variant="outlined"
						classes={{
							root: classes.textField,
						}}
						InputLabelProps={{
							shrink: true,
						}}
					></TextField>
					<TextField
						placeholder="Type your nickname"
						label="Nickname"
						variant="outlined"
						onChange={(e: ChangeEvent<HTMLInputElement>) =>
							setNickname(e.target.value.trim())
						}
						error={nicknameError.error}
						helperText={nicknameError.text}
						value={nickname}
						classes={{
							root: classes.textField,
						}}
						InputLabelProps={{
							shrink: true,
						}}
					></TextField>
					<TextField
						placeholder="Type your password"
						label="Password"
						onChange={(e: ChangeEvent<HTMLInputElement>) =>
							setPassword(e.target.value.trim())
						}
						error={passwordError.error}
						helperText={passwordError.text}
						value={password}
						variant="outlined"
						classes={{
							root: classes.textField,
						}}
						InputLabelProps={{
							shrink: true,
						}}
					></TextField>
					<Button
						variant="outlined"
						type="submit"
						classes={{
							root: classes.submitButton,
						}}
					>
						Log In
					</Button>
					<h3 className={classes.SignUp__helper}>
						Already have account?{" "}
						<Link href={"/signin"}>
							<a className={classes.SignUp__link}>Sign In.</a>
						</Link>
					</h3>
				</div>
			)}
		</form>
	);
};
