import { Button, TextField } from "@material-ui/core";
import React, { ChangeEvent, FormEvent, useState } from "react";
import Router from "next/router";
import Link from "next/link";
import Cookies from "js-cookie";
import { User } from "../../../interfaces/user";
import { useStyles } from "./styles";
import { validator, Error } from "../../../utils/validator";
import { Loader } from "../../Loader/Loader";
export interface SignInProps {
	onSignIn: (user: User) => void;
}
export const SignIn: React.FC<SignInProps> = () => {
	const classes = useStyles();
	const [loading, setLoading] = useState(false);
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [emailError, setEmailError] = useState<Error>({
		error: false,
		text: "",
	});
	const [passwordError, setPasswordError] = useState<Error>({
		error: false,
		text: "",
	});
	function checkForm() {
		validator.clear();
		validator.checkEmail(email);
		validator.checkPassword(password);
		setEmailError({
			error: !!validator.emailError,
			text: validator.emailError,
		});
		setPasswordError({
			error: !!validator.passwordError,
			text: validator.passwordError,
		});
	}
	async function submitHandler(e: FormEvent<HTMLFormElement>) {
		e.preventDefault();
		checkForm();
		if (validator.valid) {
			setLoading(true);
			const loginApi = await fetch(`/api/auth`, {
				method: "POST",
				headers: {
					Accept: "application/json",
					"Content-Type": "application/json",
				},
				body: JSON.stringify({
					email,
					password,
				}),
			}).catch((error) => {
				console.error("Error:", error);
			});
			const result = await (loginApi as Response).json();
			if (result.success && result.token) {
				Cookies.set("token", result.token, {
					expires: 30,
				});
				// window.location.href = referer ? referer : "/";
				// const pathUrl = referer ? referer.lastIndexOf("/") : "/";
				Router.push("/");
			} else {
				setEmailError(result.email);
				setPasswordError(result.password);
				setLoading(false);
			}
		}
	}
	return (
		<form className={classes.signIn} method="POST" onSubmit={submitHandler}>
			<h1 className={classes.signIn__header}>Cyber-Audio</h1>
			{loading && <Loader style={{ marginBottom: "100px" }} />}
			{!loading && (
				<div className={classes.SignIn__inner}>
					<TextField
						placeholder="Type your email"
						error={emailError.error}
						helperText={emailError.text}
						onChange={(e: ChangeEvent<HTMLInputElement>) =>
							setEmail(e.target.value.trim())
						}
						value={email}
						label="Email"
						variant="outlined"
						classes={{
							root: classes.textField,
						}}
						InputLabelProps={{
							shrink: true,
						}}
					></TextField>
					<TextField
						placeholder="Type your password"
						onChange={(e: ChangeEvent<HTMLInputElement>) =>
							setPassword(e.target.value.trim())
						}
						error={passwordError.error}
						helperText={passwordError.text}
						value={password}
						label="Password"
						variant="outlined"
						classes={{
							root: classes.textField,
						}}
						InputLabelProps={{
							shrink: true,
						}}
					></TextField>
					<Button
						variant="outlined"
						classes={{
							root: classes.submitButton,
						}}
						type="submit"
					>
						Log In
					</Button>
					<h3 className={classes.SignIn__helper}>
						Don't have account?{" "}
						<Link href={"/signup"}>
							<a className={classes.SignIn__link}>Sign Up.</a>
						</Link>
					</h3>
				</div>
			)}
		</form>
	);
};
