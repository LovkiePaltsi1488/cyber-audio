import { makeStyles } from "@material-ui/core";

export const useStyles = makeStyles((theme) => ({
	signIn: {
		zIndex: 2,
		position: "relative",
		flex: 1,
		width: "100%",
		height: "100%",
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
	},
	signIn__header: {
		color: "#fff3e0",
		boxSizing: "border-box",
		textAlign: "center",
		fontSize: "60px",
		fontWeight: 600,
		marginTop: "60px",
		marginBottom: "90px",
		WebkitUserSelect: "none",
		letterSpacing: 2,
		textShadow:
			"0 0 0.6rem #fff3e0, 0 0 1.5rem #ffab40, -0.2rem 0.1rem 1rem #ffab40, 0.2rem 0.1rem 1rem #ffab40, 0 -0.5rem 2rem #ff6d00, 0 0.5rem 3rem #ff6d00",
	},
	SignIn__inner: {
		display: "flex",
		flexDirection: "column",
	},
	textField: {
		"& label": {
			color: "rgba(255, 255, 255, 0.5)",
			transition: "0.5s",
			"&.Mui-focused": {
				color: "#ffb74d",
			},
		},
		"&:hover label": {
			color: "white",
		},
		"& .MuiOutlinedInput-root": {
			transition: "0.5s",
			"&:hover fieldset": {
				borderColor: "white",
			},
			"&.Mui-focused fieldset": {
				borderWidth: "1px",
				borderColor: "#ffb74d",
			},
			"& input": {
				fontSize: "18px",
				lineHeight: "19px",
				color: "rgba(255, 255, 255)",
				letterSpacing: 1.3,
			},
		},
		"& .MuiOutlinedInput-notchedOutline": {
			borderColor: "rgba(255, 255, 255, 0.5)",
			transition: "0.5s",
		},
		"& .MuiFormHelperText-root": {
			color: "#d50000",
		},
		height: "80px",
		width: "300px",
		marginTop: "10px",
	},
	submitButton: {
		"&": {
			borderColor: "#ffb74d",
			height: "52px",
			color: "#ffb74d",
			transition: "0.5s",
			fontSize: "18px",
			lineHeight: "19px",
			"&:hover": {
				textShadow: "0 0 5px #ffb74d, 0 0 5px #ffb74d",
			},
			marginTop: "10px",
		},
	},
	SignIn__helper: {
		fontSize: "14px",
		lineHeight: "19px",
		color: "#fff",
		marginTop: "20px",
		textAlign: "center",
	},
	SignIn__link: {
		fontSize: "14px",
		lineHeight: "19px",
		color: "#ffb74d",
	},
	"@media (max-width: 1000px)": {
		textField: {
			"& .MuiOutlinedInput-root": {
				"& input": {
					fontSize: "16px",
				},
			},
			width: "250px",
		},
		signIn__header: {
			fontSize: "50px",
			marginTop: "40px",
			marginBottom: "70px",
		},
		submitButton: {
			"&": {
				fontSize: "16px",
			},
		},
	},
	"@media (max-width: 500px)": {
		textField: {
			"& .MuiOutlinedInput-root": {
				"& input": {
					fontSize: "14px",
				},
			},
			width: "250px",
		},
		signIn__header: {
			fontSize: "40px",
			marginBottom: "50px",
			marginTop: "140px",
		},
		submitButton: {
			"&": {
				fontSize: "14px",
			},
		},
		SignIn__helper: {
			fontSize: "12px",
			lineHeight: "16px",
		},
		SignIn__link: {
			fontSize: "12px",
			lineHeight: "16px",
		},
	},
	"@media (max-width: 320px)": {
		textField: {
			width: "200px",
		},
		signIn__header: {
			marginTop: "140px",
			fontSize: "30px",
			marginBottom: "30px",
		},
		SignIn__helper: {
			fontSize: "10px",
			lineHeight: "13px",
		},
		SignIn__link: {
			fontSize: "10px",
			lineHeight: "13px",
		},
	},
}));
