import { makeStyles } from "@material-ui/core";

export const useStyles = makeStyles((theme) => ({
	background: {
		position: "absolute",
		top: 0,
		left: "-50px",
		right: "-50px",
		bottom: 0,
		zIndex: 0,
		backgroundColor: "transparent",
		overflow: "hidden",
		display: "flex",
	},
	background__column: {
		display: "flex",
		flexDirection: "column",
		flex: 1,
		alignItems: "center",
		boxSizing: "border-box",
		overflow: "hidden",
		maxHeight: "700px",
	},
	["background__column_ts-y"]: {
		height: "700px",
		transform: "translateY(-50px)",
	},
	background__circle: {
		borderRadius: "50%",
		maxWidth: "20px",
		maxHeight: "20px",
		margin: "40px",
		height: "100%",
		width: "100%",
		backgroundColor: "#FFD54F",
		zIndex: 0,
	},
	background__circle_red: {
		backgroundColor: "#E57373",
	},
	background__circle_pink: {
		backgroundColor: "#F06292",
	},
	background__circle_green: {
		backgroundColor: "#81C784",
	},
	background__circle_lime: {
		backgroundColor: "#DCE775",
	},
	background__circle_indigo: {
		backgroundColor: "#7986CB",
	},
	background__circle_orange: {
		backgroundColor: "#FFB74D",
	},
	background__circle_blue: {
		backgroundColor: "#64B5F6",
	},
}));
