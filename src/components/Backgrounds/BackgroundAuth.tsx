import React from "react";
import { useStyles } from "./styles";
export const BackgroundAuth: React.FC<{}> = () => {
	const classes = useStyles();
	return (
		<div className={classes.background}>
			<div
				className={
					classes.background__column + " " + classes["background__column_ts-y"]
				}
			>
				<span className={classes.background__circle}></span>
				<span
					className={
						classes.background__circle + " " + classes.background__circle_blue
					}
				></span>
				<span
					className={
						classes.background__circle + " " + classes.background__circle_pink
					}
				></span>
				<span
					className={
						classes.background__circle + " " + classes.background__circle_indigo
					}
				></span>
				<span
					className={
						classes.background__circle + " " + classes.background__circle_pink
					}
				></span>
				<span
					className={
						classes.background__circle + " " + classes.background__circle_blue
					}
				></span>
				<span className={classes.background__circle}></span>
			</div>

			<div className={classes.background__column}>
				<span
					className={
						classes.background__circle + " " + classes.background__circle_red
					}
				></span>
				<span
					className={
						classes.background__circle + " " + classes.background__circle_orange
					}
				></span>
				<span
					className={
						classes.background__circle + " " + classes.background__circle_lime
					}
				></span>
				<span
					className={
						classes.background__circle + " " + classes.background__circle_green
					}
				></span>
				<span
					className={
						classes.background__circle + " " + classes.background__circle_orange
					}
				></span>
				<span
					className={
						classes.background__circle + " " + classes.background__circle_red
					}
				></span>
			</div>

			<div
				className={
					classes.background__column + " " + classes["background__column_ts-y"]
				}
			>
				<span className={classes.background__circle}></span>
				<span
					className={
						classes.background__circle + " " + classes.background__circle_blue
					}
				></span>
				<span
					className={
						classes.background__circle + " " + classes.background__circle_pink
					}
				></span>
				<span
					className={
						classes.background__circle + " " + classes.background__circle_indigo
					}
				></span>
				<span
					className={
						classes.background__circle + " " + classes.background__circle_pink
					}
				></span>
				<span
					className={
						classes.background__circle + " " + classes.background__circle_blue
					}
				></span>
				<span className={classes.background__circle}></span>
			</div>

			<div className={classes.background__column}>
				<span
					className={
						classes.background__circle + " " + classes.background__circle_red
					}
				></span>
				<span
					className={
						classes.background__circle + " " + classes.background__circle_orange
					}
				></span>
				<span
					className={
						classes.background__circle + " " + classes.background__circle_green
					}
				></span>
				<span
					className={
						classes.background__circle + " " + classes.background__circle_lime
					}
				></span>
				<span
					className={
						classes.background__circle + " " + classes.background__circle_orange
					}
				></span>
				<span
					className={
						classes.background__circle + " " + classes.background__circle_red
					}
				></span>
			</div>

			<div
				className={
					classes.background__column + " " + classes["background__column_ts-y"]
				}
			>
				<span className={classes.background__circle}></span>
				<span
					className={
						classes.background__circle + " " + classes.background__circle_blue
					}
				></span>
				<span
					className={
						classes.background__circle + " " + classes.background__circle_pink
					}
				></span>
				<span
					className={
						classes.background__circle + " " + classes.background__circle_indigo
					}
				></span>
				<span
					className={
						classes.background__circle + " " + classes.background__circle_pink
					}
				></span>
				<span
					className={
						classes.background__circle + " " + classes.background__circle_blue
					}
				></span>
				<span className={classes.background__circle}></span>
			</div>

			<div className={classes.background__column}>
				<span
					className={
						classes.background__circle + " " + classes.background__circle_red
					}
				></span>
				<span
					className={
						classes.background__circle + " " + classes.background__circle_orange
					}
				></span>
				<span
					className={
						classes.background__circle + " " + classes.background__circle_lime
					}
				></span>
				<span
					className={
						classes.background__circle + " " + classes.background__circle_green
					}
				></span>

				<span
					className={
						classes.background__circle + " " + classes.background__circle_orange
					}
				></span>
				<span
					className={
						classes.background__circle + " " + classes.background__circle_red
					}
				></span>
			</div>

			<div
				className={
					classes.background__column + " " + classes["background__column_ts-y"]
				}
			>
				<span className={classes.background__circle}></span>
				<span
					className={
						classes.background__circle + " " + classes.background__circle_blue
					}
				></span>
				<span
					className={
						classes.background__circle + " " + classes.background__circle_pink
					}
				></span>
				<span
					className={
						classes.background__circle + " " + classes.background__circle_indigo
					}
				></span>
				<span
					className={
						classes.background__circle + " " + classes.background__circle_pink
					}
				></span>
				<span
					className={
						classes.background__circle + " " + classes.background__circle_blue
					}
				></span>
				<span className={classes.background__circle}></span>
			</div>

			<div className={classes.background__column}>
				<span
					className={
						classes.background__circle + " " + classes.background__circle_red
					}
				></span>
				<span
					className={
						classes.background__circle + " " + classes.background__circle_orange
					}
				></span>
				<span
					className={
						classes.background__circle + " " + classes.background__circle_green
					}
				></span>
				<span
					className={
						classes.background__circle + " " + classes.background__circle_lime
					}
				></span>

				<span
					className={
						classes.background__circle + " " + classes.background__circle_orange
					}
				></span>
				<span
					className={
						classes.background__circle + " " + classes.background__circle_red
					}
				></span>
			</div>

			<div
				className={
					classes.background__column + " " + classes["background__column_ts-y"]
				}
			>
				<span className={classes.background__circle}></span>
				<span
					className={
						classes.background__circle + " " + classes.background__circle_blue
					}
				></span>
				<span
					className={
						classes.background__circle + " " + classes.background__circle_pink
					}
				></span>
				<span
					className={
						classes.background__circle + " " + classes.background__circle_indigo
					}
				></span>
				<span
					className={
						classes.background__circle + " " + classes.background__circle_pink
					}
				></span>
				<span
					className={
						classes.background__circle + " " + classes.background__circle_blue
					}
				></span>
				<span className={classes.background__circle}></span>
			</div>

			<div className={classes.background__column}>
				<span
					className={
						classes.background__circle + " " + classes.background__circle_red
					}
				></span>
				<span
					className={
						classes.background__circle + " " + classes.background__circle_orange
					}
				></span>

				<span
					className={
						classes.background__circle + " " + classes.background__circle_lime
					}
				></span>
				<span
					className={
						classes.background__circle + " " + classes.background__circle_green
					}
				></span>
				<span
					className={
						classes.background__circle + " " + classes.background__circle_orange
					}
				></span>
				<span
					className={
						classes.background__circle + " " + classes.background__circle_red
					}
				></span>
			</div>

			<div
				className={
					classes.background__column + " " + classes["background__column_ts-y"]
				}
			>
				<span className={classes.background__circle}></span>
				<span
					className={
						classes.background__circle + " " + classes.background__circle_blue
					}
				></span>
				<span
					className={
						classes.background__circle + " " + classes.background__circle_pink
					}
				></span>
				<span
					className={
						classes.background__circle + " " + classes.background__circle_indigo
					}
				></span>
				<span
					className={
						classes.background__circle + " " + classes.background__circle_pink
					}
				></span>
				<span
					className={
						classes.background__circle + " " + classes.background__circle_blue
					}
				></span>
				<span className={classes.background__circle}></span>
			</div>
		</div>
	);
};
