import firebase from "firebase";
import "firebase/storage";
(global as any).XMLHttpRequest = require("xhr2");
try {
	firebase.initializeApp(process.env.FIREBASE_CONFIG);
} catch (err) {
	if (!/already exists/.test(err.message)) {
		console.error("Firebase initialization error", err.stack);
	}
}

export const fire = firebase;
