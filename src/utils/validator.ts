export const validator = {
	valid: true,
	emailError: "",
	passwordError: "",
	nicknameError: "",
	clear: function () {
		this.valid = true;
		this.passwordError = this.nicknameError = this.emailError = "";
	},
	checkNickname: function (nickname: string) {
		if (!nickname) {
			this.valid = false;
			this.nicknameError = "Nickname is required!";
		}
	},
	checkEmail: function (email: string) {
		if (!email) {
			this.valid = false;
			this.emailError = "Email is required!";
		} else {
			!email.match(/^[a-zA-Z0-9]+@[a-z]+.[a-z]+$/) &&
				(this.emailError = "Wrong email format!") &&
				(this.valid = false);
		}
	},
	checkPassword: function (password: string) {
		if (!password) {
			this.valid = false;
			this.passwordError = "Password is required!";
		} else if (password.length < 8) {
			this.valid = false;
			this.passwordError = "Min length is 8 symbols!";
		}
	},
};
export const musicValidator = {
	valid: true,
	titleError: "",
	authorError: "",
	songError: "",
	clear: function () {
		this.valid = true;
		this.titleError = this.authorError = this.songError = "";
	},
	checkTitle: function (title: string) {
		if (!title) {
			this.valid = false;
			this.titleError = "Title is required";
		}
	},
	checkAuthor: function (author: string) {
		if (!author) {
			this.valid = false;
			this.authorError = "Author is required";
		}
	},
	checkSong: function (song: File) {
		if (!song) {
			this.valid = false;
			this.songError = "Song is required";
		}
	},
};
export interface Error {
	error: boolean;
	text: string;
}
