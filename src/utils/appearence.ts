import { MouseEvent } from "react";
export function createRipple(e: MouseEvent<HTMLElement>) {
	e.persist();
	const target = e.currentTarget;
	const circle = document.createElement("span");
	const diameter = Math.max(target.clientWidth, target.clientHeight);
	const radius = diameter / 2;

	circle.style.width = circle.style.height = `${diameter}px`;
	circle.style.left = `${e.nativeEvent.offsetX - radius}px`;
	circle.style.top = `${e.nativeEvent.offsetY - radius}px`;
	circle.id = "ripple";
	const ripple = target.querySelectorAll("#ripple")[0];

	if (ripple) {
		ripple.remove();
	}

	target.appendChild(circle);
}
