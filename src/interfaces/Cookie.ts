export interface CookiePeaces {
	token?: string;
	[key: string]: any;
}
