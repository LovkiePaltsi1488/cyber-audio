export interface AudioFile {
	title: string;
	author: string;
	image: boolean;
	files: fileData[];
}

export interface fileData {
	buffer: Buffer;
	contentType: string;
	key: string;
}

export const keys = {
	["image/"]: "/images",
	["audio/"]: "/audios",
};
