import { NextPageContext } from "next";
import Router from "next/router";
import React, { useState } from "react";
import { Loader } from "../../src/components/Loader/Loader";
import { AppLayout } from "../../src/layouts/AppLayout/AppLayout";
import { getAppCookies, verifyToken } from "../../src/middleware/utils";

export interface IndexProps {
	profile: string;
}
export default function Index({ profile }: IndexProps) {
	const [loading, setLoading] = useState(true);
	return (
		<>
			<AppLayout title="Cyber-Audio | Library">
				{loading && <Loader />}
			</AppLayout>
		</>
	);
}

export async function getServerSideProps(context: NextPageContext) {
	const { req } = context;
	const { token } = getAppCookies(req);
	const profile = token ? verifyToken(token.split(" ")[1]) : "";
	const redirectOnError = (path) =>
		typeof window !== "undefined"
			? Router.push(`/${path}`)
			: context.res.writeHead(302, { Location: `/${path}` }).end();
	!profile && redirectOnError("signin");
	return {
		props: {
			profile,
		},
	};
}
