import { makeid } from "./../../src/utils/request";
import bcrypt from "bcryptjs";
import jwt from "jsonwebtoken";
import { NextApiRequest, NextApiResponse } from "next";
import { fire } from "../../src/config/firebase.config";
/* JWT secret key */
const KEY = process.env.JWT_KEY;

export default async (req: NextApiRequest, res: NextApiResponse) => {
	const { method } = req;
	try {
		switch (method) {
			case "POST":
				/* Get Post Data */
				const { email, password, nickname } = req.body;
				/* Any how email or password is blank */
				if (!email || !password) {
					return res.status(400).json({
						status: "error",
						email: {
							error: !!email,
							text: email ? "" : "Email is required",
						},
						password: {
							error: !!password,
							text: password ? "" : "Password is required",
						},
					});
				}
				if (nickname) {
					/* Check user email in database */
					const snapshot = await fire
						.firestore()
						.collection("users")
						.where("email", "==", email)
						.get();
					const user = snapshot.docs[0]?.data() || null;
					/* Check if exists */
					if (user) {
						return res.status(400).json({
							status: "error",
							email: {
								error: true,
								text: "User is already exist",
							},
							password: {
								error: false,
								text: "",
							},
						});
					}
					const userId = makeid(20),
						userEmail = email,
						userPassword = await bcrypt.hash(password, 12),
						userCreated = Date.now().toLocaleString();
					fire.firestore().collection("users").doc(`${userId}`).set({
						email: userEmail,
						createdAt: userCreated,
						nickname,
						password: userPassword,
						id: userId,
					});
					const payload = {
						id: userId,
						email: userEmail,
						createdAt: userCreated,
					};
					jwt.sign(
						payload,
						KEY,
						{
							expiresIn: 2592000, // 30 days in seconds
						},
						(err, token) => {
							return res.status(201).json({
								success: true,
								token: "Bearer " + token,
								email: {
									error: false,
									text: "",
								},
								password: {
									error: false,
									text: "",
								},
							});
						}
					);
				} else {
					const snapshot = await fire
						.firestore()
						.collection("users")
						.where("email", "==", email)
						.get();
					const user = snapshot.docs[0]?.data() || null;
					/* Check if exists */
					if (!user) {
						return res.status(400).json({
							status: "error",
							email: {
								error: true,
								text: "User doesn't exist",
							},
							password: {
								error: false,
								text: "",
							},
						});
					}
					const userId = user.id,
						userEmail = user.email,
						userPassword = user.password,
						userCreated = user.createdAt;
					bcrypt.compare(password, userPassword).then((isMatch) => {
						if (isMatch) {
							const payload = {
								id: userId,
								email: userEmail,
								createdAt: userCreated,
							};

							jwt.sign(
								payload,
								KEY,
								{
									expiresIn: 2592000, // 30 days in seconds
								},
								(err, token) => {
									return res.status(200).json({
										success: true,
										token: "Bearer " + token,
										email: {
											error: false,
											text: "",
										},
										password: {
											error: false,
											text: "",
										},
									});
								}
							);
						} else {
							return res.status(400).json({
								status: "error",
								email: {
									error: false,
									text: "",
								},
								password: {
									error: true,
									text: "Wrong password",
								},
							});
						}
					});
				}
				break;
			case "PUT":
				break;
			case "PATCH":
				break;
			default:
				break;
		}
	} catch (error) {
		res.status(400).json({
			status: "error",
			email: {
				error: true,
				text: "Something went wrong",
			},
			password: {
				error: true,
				text: "Something went wrong",
			},
		});
	}
};
