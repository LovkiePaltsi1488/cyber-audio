import { makeid } from "./../../src/utils/request";
import { keys } from "./../../src/interfaces/storage";
import { NextApiResponse, NextApiRequest } from "next";
import { fire } from "../../src/config/firebase.config";
import Busboy from "busboy";
import { AudioFile, fileData } from "../../src/interfaces/storage";
import {
	getAppCookies,
	getTokenData,
	verifyToken,
} from "../../src/middleware/utils";
export const config = {
	api: {
		bodyParser: false,
	},
};

export default async (request: NextApiRequest, response: NextApiResponse) => {
	let { token } = getAppCookies(request);
	token = token?.split(" ")[1] || "";
	if (verifyToken(token)) {
		const storage = fire.storage();
		const store = fire.firestore();
		const id = makeid(20);
		const audioFile: AudioFile = {
			title: "",
			author: "",
			image: false,
			files: [],
		};
		const busboy = new Busboy({ headers: request.headers });
		const { id: userId } = getTokenData(token);
		if (!userId) {
			return response
				.status(400)
				.json({ status: "error", message: "Wrong token. Log out." });
		}
		busboy.on("file", function (fieldname, file, filename, encoding, mimetype) {
			const buffer: Buffer[] = [];

			file.on("data", function (data: Buffer) {
				buffer.push(data);
			});

			file.on("end", function () {
				const fileBuffer = Buffer.concat(buffer);
				const key = mimetype.match(/^[a-z]+\//)[0];
				key === "image/" && (audioFile.image = true);
				audioFile.files.push({
					buffer: fileBuffer,
					contentType: mimetype,
					key,
				});
			});
		});
		busboy.on("field", function (fieldname, val) {
			audioFile[fieldname] = val;
		});
		busboy.on("finish", async function () {
			audioFile.files.forEach((file: fileData) => {
				storage
					.ref(keys[file.key])
					.child(`${id}`)
					.put(file.buffer, {
						contentType: file.contentType,
					})
					.catch((err) => console.log(err));
			});
			store
				.collection("users")
				.doc(userId)
				.collection("audios")
				.doc(id)
				.set({
					title: audioFile.title,
					author: audioFile.author,
					id,
					image: audioFile.image ? `/images/${id}` : null,
					audio: `/audios/${id}`,
					creatorId: userId,
				})
				.catch((err) => console.log(err));
			store
				.collection("audios")
				.doc(id)
				.set({
					title: audioFile.title,
					author: audioFile.author,
					id,
					image: audioFile.image ? `/images/${id}` : null,
					audio: `/audios/${id}`,
					creatorId: userId,
				});
			response.status(200).json({ message: "success" });
		});
		request.pipe(busboy);
	} else {
		/* response.writeHead(302, { Location: `/signin` }).end(); */
		return response.status(401).json({ message: "Need sign in" });
	}
};
