import { NextPageContext } from "next";
import Router from "next/router";
import { SignUp } from "../../src/components/Auth/SignUp/SignUp";
import { SignLayout } from "../../src/layouts/SignLayout/SignLayout";
import { getAppCookies, verifyToken } from "../../src/middleware/utils";

export default function Index() {
	return (
		<>
			<SignLayout title="SignUp">
				<SignUp />
			</SignLayout>
		</>
	);
}

export async function getServerSideProps(context: NextPageContext) {
	const { req } = context;
	const { token } = getAppCookies(req);
	const profile = token ? verifyToken(token.split(" ")[1]) : "";
	const redirectOnError = (path?) =>
		typeof window !== "undefined"
			? Router.push(`/`)
			: context.res.writeHead(302, { Location: `/` }).end();
	profile && redirectOnError();
	return {
		props: {},
	};
}
