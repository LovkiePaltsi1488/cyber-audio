import { NextPageContext } from "next";
import Router from "next/router";
import { SignIn } from "../../src/components/Auth/SIgnIn/SignIn";
import { User } from "../../src/interfaces/user";
import { SignLayout } from "../../src/layouts/SignLayout/SignLayout";
import { getAppCookies, verifyToken } from "../../src/middleware/utils";

export default function Index() {
	function signInUser(user: User) {
		console.log(user);
	}
	return (
		<>
			<SignLayout title="SignIn">
				<SignIn onSignIn={signInUser} />
			</SignLayout>
		</>
	);
}

export async function getServerSideProps(context: NextPageContext) {
	const { req } = context;
	const { token } = getAppCookies(req);
	const profile = token ? verifyToken(token.split(" ")[1]) : "";
	const redirectOnError = (path?) =>
		typeof window !== "undefined"
			? Router.push(`/`)
			: context.res.writeHead(302, { Location: `/` }).end();
	profile && redirectOnError();
	return {
		props: {},
	};
}
