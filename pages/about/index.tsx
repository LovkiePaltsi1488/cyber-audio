import { NextPageContext } from "next";
import Head from "next/head";
import Router from "next/router";
export interface IndexProps {
	baseApiUrl: string;
	profile: string;
}
export default function Index() {
	return (
		<>
			<Head>
				<title>CyberAudio | About</title>
			</Head>
			<h1>About page</h1>
		</>
	);
}

export async function getServerSideProps(context: NextPageContext) {
	const redirectOnError = (path) =>
		typeof window !== "undefined"
			? Router.push(`/${path}`)
			: context.res.writeHead(302, { Location: `/${path}` }).end();
	// redirectOnError('Signin');
	return {
		props: {},
	};
}
